/* eslint-disable no-undef */
import React, {PropTypes} from 'react';
import './Autocomplete.css';

export default class AutocompleteGooglePlaces extends React.Component {
  static propTypes = {
    onPlaceSelected: PropTypes.func,
    types: PropTypes.array,
    componentRestrictions: PropTypes.object,
    bounds: PropTypes.object,
    errorText: PropTypes.string
  }

  constructor(props) {
    super(props);
    this.autocomplete = null;
    this.state = {isError:false};
  }

  componentDidMount() {
    const { types=[], componentRestrictions, bounds } = this.props;
    const config = {types,bounds}
    if (componentRestrictions) {config.componentRestrictions = componentRestrictions}

    this.autocomplete = new google.maps.places.Autocomplete(this.refs.input, config);
    this.autocomplete.addListener('place_changed', this.onSelected.bind(this));
  }

  onSelected() {
    const place = this.autocomplete.getPlace();
    if(!place.types)this.setState({isError:true});
    if (this.props.onPlaceSelected) {
      this.props.onPlaceSelected(place);
    }
  }

  render() {
    const {errorText="Please select a valid location", onPlaceSelected, types, componentRestrictions, bounds, ...rest} = this.props;

    return (
      <div className='container'>
        <input onChange={()=>this.setState({isError:false})}
          ref="input"
          {...rest}
        />
        <div className={'error '.concat(this.state.isError?'visible':'hidden')}>{errorText}</div>
      </div>
    )
  }
}
